import javafx.scene.media.Media;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class Server {
    private ServerSocket serverSocket;
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private FirstStage firstStage;

    public Server(FirstStage firstStage) {
        this.firstStage = firstStage;
    }

    public void run() throws IOException {
        serverSocket = new ServerSocket(8568, 15);
        while (true) {
            waitForConnection();
            setupStreams();
            handleConnection();
        }
    }

    private void handleConnection() throws IOException {
        try {
            List<String> stringList = (List<String>) inputStream.readObject();
            System.out.println(stringList.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(inputStream);
        while(scanner.hasNext()) {
            String message = scanner.nextLine();
            System.out.println("Connected says " + message);
            firstStage.setMediaAndPlay(new Media(new File(message).toURI().toString()));
        }
    }

    private void setupStreams() throws IOException {
        System.out.println("Setting up streams...");
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        outputStream.flush();
        outputStream.writeObject(firstStage.fetchListOfMedia());
        System.out.println(socket.getInputStream());
        InputStream inputStreamtemp = socket.getInputStream();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStreamtemp);
        inputStream = new ObjectInputStream(bufferedInputStream);
        System.out.println("Streams setup completed successfully");
    }

    private void waitForConnection() throws IOException {
        System.out.println("Waiting for a connection...");
        socket = serverSocket.accept();
        System.out.println("Now connected to " + socket.getInetAddress().getHostName());
    }
}
