import javafx.application.Application;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.Scanner;

public class MainClass extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private static void runServer(FirstStage firstStage) {
        Server server = new Server(firstStage);
        try {
            server.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FirstStage firstStage = new FirstStage(primaryStage);
        firstStage.show();
        firstStage.setMediaAndPlay(new Media(new File("C:\\Users\\lukak\\Documents\\Ed Sheeran - I See Fire (Kygo Remix).mp4").toURI().toString()));
        new Thread(() -> {
            runServer(firstStage);
        }).start();
    }
}