import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class FirstStage {
    private Stage primaryStage;
    private Media media;
    private MediaPlayer mediaPlayer;
    private BorderPane borderPane;
    private List<String> listOfMedia;

    public FirstStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
        listOfMedia = new ArrayList<>();
        populateListOfMedia();
    }

    public void show() {
        primaryStage.setTitle("Multimedia Player");
        borderPane = new BorderPane();
        Scene scene = new Scene(borderPane);
        scene.getStylesheets().add("firstStageStyle.css");
        primaryStage.setScene(scene);

        HBox bottomHBox = new HBox();
        bottomHBox.setAlignment(Pos.CENTER);
        bottomHBox.getStyleClass().add("hbox");
        borderPane.setBottom(bottomHBox);

        Button buttonPause = new Button();
        buttonPause.getStyleClass().add("hbox");
        bottomHBox.getChildren().add(buttonPause);
        Image imagePause = new Image(getClass().getResourceAsStream("pause.png"));
        ImageView pauseView = new ImageView(imagePause);
        pauseView.setFitWidth(50);
        pauseView.setFitHeight(50);
        buttonPause.setGraphic(pauseView);
        buttonPause.setOnAction((e) -> {
            pauseResumeMedia();
        });

        Button buttonPlay = new Button();
        buttonPlay.getStyleClass().add("hbox");
        bottomHBox.getChildren().add(buttonPlay);
        Image imagePlay = new Image(getClass().getResourceAsStream("play.png"));
        ImageView playView = new ImageView(imagePlay);
        playView.setFitHeight(60);
        playView.setFitWidth(60);
        buttonPlay.setGraphic(playView);
        buttonPlay.setOnAction((e) -> {
            playMedia();
        });

        Button buttonStop = new Button();
        buttonStop.getStyleClass().add("hbox");
        bottomHBox.getChildren().add(buttonStop);
        Image imageStop = new Image(getClass().getResourceAsStream("cancel.png"));
        ImageView stopView = new ImageView(imageStop);
        stopView.setFitWidth(50);
        stopView.setFitHeight(50);
        buttonStop.setGraphic(stopView);
        buttonStop.setOnAction((e) -> {
            stopMedia();
        });

        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(0.3), event -> {
            MediaView mediaView = new MediaView(mediaPlayer);
            borderPane.setCenter(mediaView);
        }));
        fiveSecondsWonder.setCycleCount(Timeline.INDEFINITE);
        fiveSecondsWonder.play();

        String IPAdress = "";
        try {
            IPAdress = Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        Label labelIPAdress = new Label("Your IP address is " + IPAdress + ", port: 8568");
        borderPane.setTop(labelIPAdress);

        ListView<String> list = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList(
                listOfMedia);
        list.setItems(items);
        list.setOnMouseClicked((e) -> {
            String item = list.getSelectionModel().getSelectedItem();
            setMediaAndPlay(new Media(new File(item).toURI().toString()));
        });
        borderPane.setLeft(list);

        primaryStage.show();
    }

    private void populateListOfMedia() {
        File folder = new File(".");
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile())
                if (listOfFiles[i].getName().toLowerCase().endsWith("mp4") || listOfFiles[i].getName().toLowerCase().endsWith("mp3"))
                    listOfMedia.add(listOfFiles[i].getName());
        }
    }

    public void pauseResumeMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
        }
    }

    public void stopMedia() {
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    public void playMedia() {
        mediaPlayer.play();
    }

    public void setMediaAndPlay(Media media) {
        this.media = media;
        if (mediaPlayer != null)
            mediaPlayer.stop();
        mediaPlayer = new MediaPlayer(media);
        playMedia();
    }

    public List<String> fetchListOfMedia() {
        return listOfMedia;
    }
}
