import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestServerClass {

    public static void main(String[] args) {
        final String host = "localhost";
        final int portNumber = 8568;
        System.out.println("Creating socket to '" + host + "' on port " + portNumber);
        try {
            while (true) {
                Socket socket = new Socket(host, portNumber);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                List<String> listOfStrings = new ArrayList<>();
                listOfStrings.add("Zagreb");
                listOfStrings.add("Split");
                objectOutputStream.writeObject(listOfStrings);
                ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                List<String> listOfMedia = (List<String>) objectInputStream.readObject();
                System.out.println(listOfMedia.toString());
                PrintWriter out = new PrintWriter(objectOutputStream, true);

                Scanner scanner = new Scanner(System.in);
                while (scanner.hasNext()) {
                    String userInput = scanner.nextLine();
                    out.println(userInput);
                    out.flush();

                    if ("exit".equalsIgnoreCase(userInput)) {
                        socket.close();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
