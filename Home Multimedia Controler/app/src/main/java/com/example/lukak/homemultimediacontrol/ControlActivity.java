package com.example.lukak.homemultimediacontrol;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ControlActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        ListView listView = findViewById(R.id.listView_medias);
        final List<String> list = getIntent().getExtras().getStringArrayList("list");
        final ArrayAdapter<String> aAdpt =
                new ArrayAdapter<String>(this,
                        android.R.layout.simple_list_item_1, list);
        listView.setAdapter(aAdpt);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String media = list.get(position);
                new LongOperation().execute(ControlActivity.this.getIntent().getExtras().getString("host"),
                        ControlActivity.this.getIntent().getExtras().getString("port"),
                        media);
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            System.out.println("Here");
            Socket socket = null;
            try {
                socket = new Socket(InetAddress.getByName(strings[0]), Integer.parseInt(strings[1]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            List<String> listOfStrings = new ArrayList<>();
            listOfStrings.add("Zagreb");
            listOfStrings.add("Split");
            try {
                objectOutputStream.writeObject(listOfStrings);
            } catch (IOException e) {
                e.printStackTrace();
            }
            new PrintWriter(objectOutputStream, true).println(strings[2]);
            System.out.println(strings[2]);
            try {
                objectOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
