package com.example.lukak.homemultimediacontrol;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ArrayList<String> listOfMedias = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText ipAddressEdit = findViewById(R.id.textView_address);
        final EditText portEdit = findViewById(R.id.textView_port);
        Button connectButton = findViewById(R.id.button_connect);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String host = ipAddressEdit.getText().toString();
                String portNumber = portEdit.getText().toString();
                new LongOperation(MainActivity.this).execute(host, portNumber);
            }
        });
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        public Activity myActivity;
        public String host;
        public String port;

        public LongOperation(Activity myActivity) {
            this.myActivity = myActivity;
        }

        @Override
        protected String doInBackground(String... strings) {
            Socket socket = null;
            host = strings[0];
            port = strings[1];
            try {
                socket = new Socket(InetAddress.getByName(strings[0]), Integer.parseInt(strings[1]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            List<String> listOfStrings = new ArrayList<>();
            listOfStrings.add("Zagreb");
            listOfStrings.add("Split");
            try {
                objectOutputStream.writeObject(listOfStrings);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ObjectInputStream objectInputStream = null;
            try {
                objectInputStream = new ObjectInputStream(socket.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
            List<String> listOfMedia = null;
            try {
                listOfMedia = (List<String>) objectInputStream.readObject();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println(listOfMedia.toString());
            listOfMedias = (ArrayList) listOfMedia;
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result)
        {
            Intent intent = new Intent(MainActivity.this, ControlActivity.class).putExtra("list", listOfMedias);
            intent.putExtra("host", host);
            intent.putExtra("port", port);
            myActivity.startActivity(intent);
        }
    }
}
